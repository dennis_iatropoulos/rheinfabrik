//
//  RFTSearchCollectionViewCell.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 14.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RFTSearchCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UITextView *overviewTextView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *backLabel;

- (void)triggerOverview;
- (void)showOverview:(BOOL)show animated:(BOOL)animated;

@end
