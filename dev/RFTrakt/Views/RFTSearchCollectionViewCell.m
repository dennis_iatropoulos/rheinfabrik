//
//  RFTSearchCollectionViewCell.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 14.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTSearchCollectionViewCell.h"

@interface RFTSearchCollectionViewCell ()

@property (nonatomic) BOOL isShowing;

@end

@implementation RFTSearchCollectionViewCell

- (void)awakeFromNib {
	[self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[self.layer setBorderWidth:2.0];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideOverview:) name:@"HideAllOverviews" object:nil];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)triggerOverview {
	[self showOverview:!self.isShowing animated:YES];
}

- (void)showOverview:(BOOL)show animated:(BOOL)animated {
	
	if (show == self.isShowing) {
		return;
	}
	
	self.isShowing = show;
	
	if (animated) {
		CATransition *transition = [CATransition animation];
		transition.duration = 0.4f;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
		transition.type = @"alignedFlip";
		if (show) {
			transition.subtype = kCATransitionFromRight;
		} else {
			transition.subtype = kCATransitionFromLeft;
		}
		[self.layer addAnimation:transition forKey:@"LevelTransition"];
	}
	
	if (show) {
		[self.posterImageView setAlpha:0.15f];
		[self.overviewTextView setAlpha:1.0f];
		[self.backLabel setAlpha:1.0f];
	} else {
		[self.posterImageView setAlpha:1.0f];
		[self.overviewTextView setAlpha:0.0f];
		[self.backLabel setAlpha:0.0f];
	}
}

- (void)hideOverview:(NSNotification *)notification {
//	[self showOverview:NO];
}

@end
