//
//  RFTSearchTableViewCell.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 13.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTSearchTableViewCell.h"

@implementation RFTSearchTableViewCell

- (void)awakeFromNib {
	[self.movieImageView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
	[self.movieImageView.layer setBorderWidth:1.0];
	[self.movieImageView setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

	
}

@end
