//
//  RFTPopularTableViewCell.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 12.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTPopularTableViewCell.h"

@implementation RFTPopularTableViewCell

- (void)awakeFromNib {
	[self.movieThumbnailActivityIndicator setHidesWhenStopped:YES];
	[self.movieThumbnailImageView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
	[self.movieThumbnailImageView.layer setBorderWidth:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	
	
}

@end
