//
//  RFTSearchTableViewCell.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 13.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RFTSearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *movieYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *movieOverviewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *movieImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *movieThumbnailActivityIndicator;

@end
