//
//  RFTPopularTableViewCell.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 12.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RFTPopularTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *moviePositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *movieThumbnailImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *movieThumbnailActivityIndicator;

@end
