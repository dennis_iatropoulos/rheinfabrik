//
//  AppDelegate.h
//  RFTrakt
//
//  Created by Dennis Iatropoulos on 10.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

