//
//  RFTTabBarViewController.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTTabBarViewController.h"

@interface RFTTabBarViewController ()

@end

@implementation RFTTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
