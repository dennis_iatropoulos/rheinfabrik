//
//  RFTTabBarViewController.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RFTTabBarViewController : UITabBarController

@end
