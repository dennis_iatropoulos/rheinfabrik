//
//  RFTSearchViewController.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTSearchViewController.h"
#import "RFTService.h"
#import "RFTSearchTableViewCell.h"

@interface RFTSearchViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) RFTService *service;
@property (nonatomic, strong) RFTSearchList *searchList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *resultCounterLabel;
@property (nonatomic) NSUInteger currentCount;
@property (nonatomic, strong) NSMutableArray *cellHeights;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation RFTSearchViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.service = [[RFTService alloc] init];
	self.resultCounterLabel.text = @"";
	
	[self.tableView setDataSource:self];
	[self.tableView setDelegate:self];
	
	[self.searchBar setDelegate:self];
	[self.searchBar setReturnKeyType:UIReturnKeyDone];
	
	[self.infoLabel setHidden:YES];
	self.infoLabel.text = @"Loaded all movies";
	self.resultCounterLabel.text = @"Found 0 movies";
	
	// Keyboard will be dismissed if view loses editing mode. Here it is only the searchBar
	[self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)]];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (self.searchList.searchModels.count == 0) {
		[self.searchBar becomeFirstResponder];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

/**
 * Calculating the different cell heights by simulating the used label in UITableViewCell
 * You have to copy the label-frame properties and font-family from the storyboard. Not the best way...
 */
- (void)calculateCellHeights {
	
	float textLabelWidth = 0.0f;
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	if (screenBounds.size.height == 736.0) {
		textLabelWidth = 285.0f;
	} else if(screenBounds.size.height == 667) {
		textLabelWidth = 258.0f;
	} else {
		textLabelWidth = 203.0f;
	}
	// From Storyboard. Please change when modified!
	float textLabelHeight = 70.0f;
	float textLabelY = 72.0f;
	float minCellHeight = 150.0f;
	
	self.cellHeights = [[NSMutableArray alloc] init];
	
	[self.searchList.searchModels enumerateObjectsUsingBlock:^(RFTSearchModel *currentSearchModel, NSUInteger idx, BOOL *stop) {
		UILabel *simulateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, textLabelWidth, textLabelHeight)];
		[simulateLabel setFont:[UIFont fontWithName:@".SFUIText-Light" size:14.0f]];
		[simulateLabel setNumberOfLines:0];
		[simulateLabel setLineBreakMode:NSLineBreakByWordWrapping];
		[simulateLabel setText:currentSearchModel.movie.overview];
		
		float width = simulateLabel.bounds.size.width;
		[simulateLabel sizeToFit];
		CGRect tempFrame = simulateLabel.frame;
		tempFrame.size.width = width;
		simulateLabel.frame = tempFrame;
		
		float calculatedHeight = simulateLabel.frame.size.height + textLabelY + 10.0f;
		if (calculatedHeight < minCellHeight) {
			calculatedHeight = minCellHeight;
		}
		
		[self.cellHeights addObject:[NSNumber numberWithFloat:calculatedHeight]];
		
		simulateLabel = nil;
	}];
}

#pragma mark - UISearchBarDelegate

/*
 * Every change in the searchBar.text triggers a new search from the service class
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	self.currentCount = 0;
	[self.infoLabel setHidden:YES];
	if (searchText.length > 0) {
		NSLog(@"%@", searchText);
		[self addMoreMoviesToListForTextquery:searchText];
	} else {
		self.searchList = nil;
		self.resultCounterLabel.text = @"Found 0 movies";
		self.navigationController.tabBarItem.badgeValue = nil;
		[self.tableView reloadData];
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

#pragma mark - Service

/**
 * Method will load movies for textquery
 * If the querytext is the same as before, the method will load the next moviews for the same textquery
 */
- (void)addMoreMoviesToListForTextquery:(NSString *)textquery {
	[self.activityIndicator startAnimating];
	[self.service requestMoreSearchMoviesForTextquery:textquery withReturnBlock:^(RFTObjectList *objectList) {
		self.searchList	= (RFTSearchList *)objectList;
		[self calculateCellHeights];
		// If there was a change in the searchBar.text we can reload the complete tabelView
		// Otherwise just add new cells to the tableview
		if (self.currentCount == 0) {
			[self.tableView reloadData];
			// When loading movies for a new textquery the tableView will sroll to Top
			if (self.searchList.searchModels.count > 0) {
				[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
			}
		} else {
			NSUInteger newElementsCount = self.searchList.searchModels.count - self.currentCount;
			// When there are no new elements, the user reachs the end of the movielist
			if (newElementsCount > 0) {
				NSMutableArray *newIndexPaths = [[NSMutableArray alloc] initWithCapacity:newElementsCount];
				for (int i=0; i<newElementsCount; i++) {
					[newIndexPaths addObject:[NSIndexPath indexPathForRow:self.currentCount+i inSection:0]];
				}
				[self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationFade];
			} else {
				[self.infoLabel setHidden:NO];
			}
		}
		[self.activityIndicator stopAnimating];
		self.currentCount = self.searchList.searchModels.count;
		self.resultCounterLabel.text = [NSString stringWithFormat:@"Showing %lu / %@ movies", (unsigned long)self.searchList.searchModels.count, self.searchList.itemCount];
		NSUInteger itemsToLoad = self.searchList.itemCount.integerValue - (int)self.searchList.searchModels.count;
		if (itemsToLoad > 0) {
			self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"+%lu", (unsigned long)itemsToLoad];
		} else {
			self.navigationController.tabBarItem.badgeValue = nil;;
		}
	} withErrorBlock:^(NSError *error) {
		[self.activityIndicator stopAnimating];
		NSLog(@"Error loading movies for search: %@", error);
	}];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.searchList.searchModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	RFTSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
	
	RFTSearchModel *currentSearchModel = [self.searchList.searchModels objectAtIndex:indexPath.row];
	
	cell.movieTitleLabel.text = currentSearchModel.movie.title;
	cell.movieYearLabel.text = @"";
	if ((int)currentSearchModel.movie.year > 0) {
		cell.movieYearLabel.text = [NSString stringWithFormat:@"%i", (int)currentSearchModel.movie.year];
	}
	cell.movieOverviewLabel.text = currentSearchModel.movie.overview;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		float width = cell.movieOverviewLabel.bounds.size.width;
		[cell.movieOverviewLabel sizeToFit];
		CGRect tempFrame = cell.movieOverviewLabel.frame;
		tempFrame.size.width = width;
		cell.movieOverviewLabel.frame = tempFrame;
	}
	
	cell.movieImageView.image = nil;
	[cell.movieThumbnailActivityIndicator startAnimating];
	
	if (indexPath.row%2) {
		[cell setBackgroundColor:[UIColor colorWithWhite:0.9f alpha:1.0f]];
	} else {
		[cell setBackgroundColor:[UIColor colorWithWhite:0.8f alpha:1.0f]];
	}
	
	/*
	 * Request local-imagepath for online-url
	 * Handle fallback in ViewController with default image and different ContentMode of imageView
	 */
	[self.service requestLocalImagePathForImageURL:[NSURL URLWithString:currentSearchModel.movie.images.poster.thumb] withImageBlock:^(NSString *localImagePath) {
		if (localImagePath.length > 0) {
			cell.movieImageView.image = [UIImage imageWithContentsOfFile:localImagePath];
			[cell.movieImageView setContentMode:UIViewContentModeScaleToFill];
		} else {
			cell.movieImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"no_image" ofType:@"jpg"]]];
			[cell.movieImageView setContentMode:UIViewContentModeScaleAspectFit];
		}
		[cell.movieThumbnailActivityIndicator stopAnimating];
		[cell setNeedsLayout];
	}];
	
	return cell;
}

#pragma mark - UITableViewDelegate

/*
 * return calculated height for each cell
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		return 225.0f;
	}
	return ((NSNumber *)[self.cellHeights objectAtIndex:indexPath.row]).floatValue;
}

#pragma mark - UIScrollViewDelegate

/*
 * User starts dragging the tableView will hide the keyboard
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.searchBar resignFirstResponder];
}

/*
 * Load more movies when user reaches the end of tableView
 * Triggering loading in "scrollViewDidEndDragging" and "scrollViewDidEndDragging"
 * "scrollViewDidScroll" triggers to much
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)checkIfUpdateScrollView:(UIScrollView *)scrollView {
	if (self.searchList.searchModels.count > 0) {
		NSInteger maxOffset = scrollView.contentSize.height - scrollView.frame.size.height;
		if (maxOffset - scrollView.contentOffset.y <= 100) {
			[self addMoreMoviesToListForTextquery:self.searchBar.text];
		}
	}
}

@end
