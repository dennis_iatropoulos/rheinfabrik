//
//  RFTSearchiPadViewController.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 14.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTSearchiPadViewController.h"
#import "RFTService.h"
#import "RFTSearchCollectionViewCell.h"

@interface RFTSearchiPadViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) RFTService *service;
@property (nonatomic, strong) RFTSearchList *searchList;
@property (nonatomic) NSUInteger currentCount;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *resultCounterLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation RFTSearchiPadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.service = [[RFTService alloc] init];
	
	[self.collectionView setDataSource:self];
	[self.collectionView setDelegate:self];
	
	[self.searchBar setDelegate:self];
	[self.searchBar setReturnKeyType:UIReturnKeyDone];
	
	self.resultCounterLabel.text = @"Found 0 movies";
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (self.searchList.searchModels.count == 0) {
		[self.searchBar becomeFirstResponder];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UISearchBarDelegate

/*
 * Every change in the searchBar.text triggers a new search from the service class
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	self.currentCount = 0;
//	[self.infoLabel setHidden:YES];
	if (searchText.length > 0) {
		NSLog(@"%@", searchText);
		[self addMoreMoviesToListForTextquery:searchText];
	} else {
		self.searchList = nil;
		self.resultCounterLabel.text = @"Found 0 movies";
		self.navigationController.tabBarItem.badgeValue = nil;
		[self.collectionView reloadData];
	}
}

#pragma mark - Service

/**
 * Method will load movies for textquery
 * If the querytext is the same as before, the method will load the next moviews for the same textquery
 */
- (void)addMoreMoviesToListForTextquery:(NSString *)textquery {
	[self.service requestMoreSearchMoviesForTextquery:textquery withReturnBlock:^(RFTObjectList *objectList) {
		self.searchList	= (RFTSearchList *)objectList;
		// If there was a change in the searchBar.text we can reload the complete tabelView
		// Otherwise just add new cells to the tableview
		if (self.currentCount == 0) {
			[self.collectionView reloadData];
			// When loading movies for a new textquery the tableView will sroll to Top
			if (self.searchList.searchModels.count > 0) {
				[self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
			}
		} else {
			NSUInteger newElementsCount = self.searchList.searchModels.count - self.currentCount;
			// When there are no new elements, the user reachs the end of the movielist
			if (newElementsCount > 0) {
				NSMutableArray *newIndexPaths = [[NSMutableArray alloc] initWithCapacity:newElementsCount];
				for (int i=0; i<newElementsCount; i++) {
					[newIndexPaths addObject:[NSIndexPath indexPathForRow:self.currentCount+i inSection:0]];
				}
				[self.collectionView insertItemsAtIndexPaths:newIndexPaths];
			}
		}
		self.currentCount = self.searchList.searchModels.count;
		self.resultCounterLabel.text = [NSString stringWithFormat:@"Showing %lu / %@ movies", (unsigned long)self.searchList.searchModels.count, self.searchList.itemCount];
		NSUInteger itemsToLoad = self.searchList.itemCount.integerValue - (int)self.searchList.searchModels.count;
		if (itemsToLoad > 0) {
			self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"+%lu", (unsigned long)itemsToLoad];
		} else {
			self.navigationController.tabBarItem.badgeValue = nil;;
		}
	} withErrorBlock:^(NSError *error) {
		NSLog(@"Error loading movies for search: %@", error);
	}];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return self.searchList.searchModels.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	RFTSearchCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchCell" forIndexPath:indexPath];
	
	RFTSearchModel *currentSearchModel = [self.searchList.searchModels objectAtIndex:indexPath.row];
	
	[cell showOverview:NO animated:NO];
	
	cell.movieTitleLabel.text = currentSearchModel.movie.title;
	cell.overviewTextView.text = currentSearchModel.movie.overview;
	cell.posterImageView.image = nil;
	[cell.activityIndicator startAnimating];
	
	/*
	 * Request local-imagepath for online-url
	 * Handle fallback in ViewController with default image and different ContentMode of imageView
	 */
	[self.service requestLocalImagePathForImageURL:[NSURL URLWithString:currentSearchModel.movie.images.poster.thumb] withImageBlock:^(NSString *localImagePath) {
		if (localImagePath.length > 0) {
			cell.posterImageView.image = [UIImage imageWithContentsOfFile:localImagePath];
			[cell.posterImageView setContentMode:UIViewContentModeScaleToFill];
		} else {
			cell.posterImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"no_image" ofType:@"jpg"]]];
			[cell.posterImageView setContentMode:UIViewContentModeScaleAspectFit];
		}
		[cell.activityIndicator stopAnimating];
		[cell setNeedsLayout];
	}];
	
	return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	[self.searchBar resignFirstResponder];
	RFTSearchCollectionViewCell *currentCell = (RFTSearchCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
	[currentCell triggerOverview];
}

#pragma mark - UIScrollViewDelegate
/*
 * User starts dragging the tableView will hide the keyboard
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.searchBar resignFirstResponder];
}

/*
 * Tell all collectionViewCells to hide their overviews
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	[[NSNotificationCenter defaultCenter] postNotificationName:@"HideAllOverviews" object:nil];
}

/*
 * Load more movies when user reaches the end of tableView
 * Triggering loading in "scrollViewDidEndDragging" and "scrollViewDidEndDragging"
 * "scrollViewDidScroll" triggers to much
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)checkIfUpdateScrollView:(UIScrollView *)scrollView {
	if (self.searchList.searchModels.count > 0) {
		NSInteger maxOffset = scrollView.contentSize.height - scrollView.frame.size.height;
		if (maxOffset - scrollView.contentOffset.y <= 300) {
			[self addMoreMoviesToListForTextquery:self.searchBar.text];
		}
	}
}

@end
