//
//  RFTPopularViewController.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTPopularViewController.h"
#import "RFTService.h"
#import "RFTPopularTableViewCell.h"

@interface RFTPopularViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) RFTService *service;
@property (nonatomic, strong) RFTPopularList *popularList;
@property (nonatomic) NSUInteger currentCount;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL isUpdating;

@end

@implementation RFTPopularViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.service = [[RFTService alloc] init];
	
	[self.tableView setDataSource:self];
	[self.tableView setDelegate:self];
	
	self.isUpdating = NO;
	
	[self addMoreMoviesToList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
 * Reload the first movie list
 */
- (IBAction)refreshButtonTouched:(UIBarButtonItem *)sender {
	[self.service cleanPopularMovies];
	self.popularList = nil;
	self.currentCount = 0;
	[self addMoreMoviesToList];
	[self.tableView reloadData];
}

#pragma mark - Service

/**
 * Load more movies from service class. Count is defined in service
 */
- (void)addMoreMoviesToList {
	if (self.isUpdating) {
		return;
	}
	self.isUpdating = YES;
	[self.activityIndicator startAnimating];
	[self.service requestMorePopularMoviesWithReturnBlock:^(RFTObjectList *objectList) {
		self.popularList = (RFTPopularList *)objectList;
		// Calculate new elementsCount and add new rows to tabelView
		NSUInteger newElementsCount = self.popularList.popularModels.count - self.currentCount;
		if (newElementsCount > 0) {
			NSMutableArray *newIndexPaths = [[NSMutableArray alloc] initWithCapacity:newElementsCount];
			for (int i=0; i<newElementsCount; i++) {
				[newIndexPaths addObject:[NSIndexPath indexPathForRow:self.currentCount+i inSection:0]];
			}
			[self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationFade];
		}
		self.currentCount = self.popularList.popularModels.count;
		[self.activityIndicator stopAnimating];
		self.isUpdating = NO;
	} withErrorBlock:^(NSError *error) {
		[self.activityIndicator stopAnimating];
		self.isUpdating = NO;
		NSLog(@"%@", error);
	}];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.popularList.popularModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	RFTPopularTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PopularCell" forIndexPath:indexPath];
	
	RFTPopularModel *currentPopularModel = [self.popularList.popularModels objectAtIndex:indexPath.row];
	
	/*
	 * Not the cleanest code here, but i dont want to use category classes from other projects
	 * So the new label origins are manipulated with a tempFrame
	 */
	cell.moviePositionLabel.text = [NSString stringWithFormat:@"%li.", indexPath.row+1];
	CGSize positionSize = [cell.moviePositionLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, cell.moviePositionLabel.bounds.size.height)];
	CGRect tempFrame = cell.moviePositionLabel.frame;
	tempFrame.size.width = positionSize.width;
	cell.moviePositionLabel.frame = tempFrame;
	cell.movieTitleLabel.text = currentPopularModel.title;
	tempFrame = cell.movieTitleLabel.frame;
	tempFrame.origin.x = cell.moviePositionLabel.frame.origin.x + cell.moviePositionLabel.frame.size.width + 4.0f;
	tempFrame.size.width = tableView.bounds.size.width - tempFrame.origin.x - 5.0f;
	cell.movieTitleLabel.frame = tempFrame;
	cell.movieThumbnailImageView.image = nil;
	[cell.movieThumbnailActivityIndicator startAnimating];
	
	/*
	 * Request local-imagepath for online-url
	 * Handle fallback in ViewController with default image and different ContentMode of imageView
	 */
	[self.service requestLocalImagePathForImageURL:[NSURL URLWithString:currentPopularModel.images.banner.full] withImageBlock:^(NSString *localImagePath) {
		if (localImagePath.length > 0) {
			cell.movieThumbnailImageView.image = [UIImage imageWithContentsOfFile:localImagePath];
			[cell.movieThumbnailImageView setContentMode:UIViewContentModeScaleToFill];
		} else {
			cell.movieThumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"no_image" ofType:@"jpg"]]];
			[cell.movieThumbnailImageView setContentMode:UIViewContentModeScaleAspectFit];
		}
		[cell.movieThumbnailActivityIndicator stopAnimating];
		[cell setNeedsLayout];
	}];
	
	return cell;
}

#pragma mark - UITableViewDelegate

/*
 * Tiny presenting animation for new cells
 */
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	cell.transform = CGAffineTransformMakeScale(1.02f, 1.02f);
	[UIView animateWithDuration:0.25f animations:^{
		cell.transform = CGAffineTransformIdentity;
	}];
}

#pragma mark - UIScrollViewDelegate

/*
 * Load more movies when user reaches the end of tableView
 * Triggering loading in "scrollViewDidEndDragging" and "scrollViewDidEndDragging"
 * "scrollViewDidScroll" triggers to much
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	[self checkIfUpdateScrollView:scrollView];
}

- (void)checkIfUpdateScrollView:(UIScrollView *)scrollView {
	NSInteger maxOffset = scrollView.contentSize.height - scrollView.frame.size.height;
	if (maxOffset - scrollView.contentOffset.y <= 100) {
		[self addMoreMoviesToList];
	}
}

@end
