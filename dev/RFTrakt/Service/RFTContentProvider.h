//
//  RFTContentProvider.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 12.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ImageBlock)(NSString *localImagePath);

@interface RFTContentProvider : NSObject

- (void)requestLocalImagePathForImageURL:(NSURL *)imageURL withImageBlock:(ImageBlock)imageBlock;

@end
