//
//  RFTService.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTService.h"
#import "RFTServiceMeta.h"

#define kPaginationCurrentPageKey @"x-pagination-page"
#define kPaginationPageCountKey	  @"x-pagination-page-count"
#define kPaginationLimitKey		  @"x-pagination-limit"
#define kPaginationItemCountKey	  @"x-pagination-item-count"

typedef enum : NSUInteger {
	RFTRequestTypePopular,
	RFTRequestTypeSearch,
} RFTRequestTypes;

@interface RFTService ()

@property (nonatomic, strong) RFTServiceMeta *serviceMeta;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionDataTask *searchTask;

@property (nonatomic, strong) RFTPopularList *popularList;
@property (nonatomic, strong) RFTSearchList *searchList;
@property (nonatomic, strong) NSString *lastSearchedTextquery;
@property (nonatomic) NSUInteger popularCurrentPage;
@property (nonatomic) NSUInteger searchCurrentPage;

@property (nonatomic, strong) RFTContentProvider *contentProvider;

@end

@implementation RFTService

- (instancetype)init {
	self = [super init];
	if (self) {
		self.serviceMeta = [[RFTServiceMeta alloc] init];
		self.popularList = [[RFTPopularList alloc] init];
		self.popularCurrentPage = 0;
		self.searchCurrentPage = 0;
		self.searchList = [[RFTSearchList alloc] init];
		self.contentProvider = [[RFTContentProvider alloc] init];
		[self prepareSession];
	}
	return self;
}

- (void)prepareSession {
	
	NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
	sessionConfig.allowsCellularAccess = YES;
	[sessionConfig setHTTPAdditionalHeaders:self.serviceMeta.params];
	sessionConfig.timeoutIntervalForRequest = 30.0;
	sessionConfig.timeoutIntervalForResource = 60.0;
	
	self.session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
}

- (void)cleanPopularMovies {
	self.popularCurrentPage = 0;
	self.popularList = [[RFTPopularList alloc] init];
}

- (void)cleanSearchMovies {
	self.searchCurrentPage = 0;
	self.searchList = [[RFTSearchList alloc] init];
}

#pragma mark - trakt.tv API
#pragma mark - Request popular movies

/*
 * Public wrapper for async requesting next "packages" of popular movies
 */
- (void)requestMorePopularMoviesWithReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock {
	[self requestPopularMoviesForPagenumber:++self.popularCurrentPage withReturnBlock:returnBlock withErrorBlock:errorBlock];
}

/*
 * Async requesting popular movies with given pagenumber
 */
- (void)requestPopularMoviesForPagenumber:(NSUInteger)pagenumber withReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock {
	NSString *requestString = [NSString stringWithFormat:@"%@?extended=full,images&page=%lu&limit=%@", self.serviceMeta.hostPopular, (unsigned long)pagenumber, self.serviceMeta.limitPopular];
	[self requestWithURL:[NSURL URLWithString:requestString] withRequestType:RFTRequestTypePopular withCompletionBlock:^() {
		if (returnBlock) {
			returnBlock(self.popularList);
		}
	} withErrorBlock:errorBlock];
}

#pragma mark - Request search movies

/*
 * Public wrapper for async requesting movies for given textquery.
 * If the textquery is the same as in the last call, method will return more movies for the same textquery in returnBlock
 */
- (void)requestMoreSearchMoviesForTextquery:(NSString *)textquery withReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock {
	if ([textquery isEqualToString:self.lastSearchedTextquery]) {
		self.searchCurrentPage++;
	} else {
		self.searchCurrentPage = 1;
		self.searchList = [[RFTSearchList alloc] init];
	}
	[self requestSearchMoviesForTextquery:textquery forPagenumber:self.searchCurrentPage withReturnBlock:returnBlock withErrorBlock:errorBlock];
}

/*
 * Async requesting movies for given textquery and pagenumber.
 */
- (void)requestSearchMoviesForTextquery:(NSString *)textquery forPagenumber:(NSUInteger)pagenumber withReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock {
	self.lastSearchedTextquery = textquery;
	NSString *requestString = [NSString stringWithFormat:@"%@?query=%@&type=movie&page=%lu&limit=%@", self.serviceMeta.hostSearch, [self.lastSearchedTextquery stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], (unsigned long)pagenumber, self.serviceMeta.limitSearch];
	[self requestWithURL:[NSURL URLWithString:requestString] withRequestType:RFTRequestTypeSearch withCompletionBlock:^() {
		if (returnBlock) {
			returnBlock(self.searchList);
		}
	} withErrorBlock:errorBlock];
}

#pragma mark - Global request

/*
 * Async API calls with completionBlock and errorBlock
 * Methods limits search-tasks to 1 at a time. If a new search-task is startet while another search-task is running, the old one will be canceled.
 */
- (void)requestWithURL:(NSURL *)url withRequestType:(RFTRequestTypes)requestType withCompletionBlock:(CompletionBlock)completionBlock withErrorBlock:(ErrorBlock)errorBlock {
	
	// Search for other running search-tasks and cancel it
	[self.session getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
		[dataTasks enumerateObjectsUsingBlock:^(NSURLSessionDataTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			if ([obj.taskDescription isEqualToString:@"SearchTask"]) {
				[obj cancel];
				NSLog(@"Cancel SearchTask with ID >>> %lu", (unsigned long)obj.taskIdentifier);
			}
		}];
	}];
	
	/*
	 * Send async request to trakt.tv API and send received data to parser
	 * Triggers completionBlock on success
	 * Triggers errorBlock on request-error or response-error
	 */
	NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:[NSURLRequest requestWithURL:url] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		if (!error) {
			NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
			if (httpResponse.statusCode == 200) {
				[self parseHTTPHeader:httpResponse.allHeaderFields withRequestType:requestType];
				[self parseJSONData:data withRequestType:requestType];
				
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					if (completionBlock) {
						completionBlock();
					}
				}];
				
			} else {
				// TODO Handle statusCodes
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					if (errorBlock) {
						errorBlock([NSError errorWithDomain:[NSString stringWithFormat:@"%@", url] code:httpResponse.statusCode userInfo:nil]);
					}
				}];
			}
		} else {
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				if (errorBlock) {
					errorBlock(error);
				}
			}];
		}
	}];
	
	// Set TaskDescription for searchTasks for new textqueries to find them later and cancel them if needed
	if (requestType == RFTRequestTypeSearch && self.searchList.searchModels.count == 0) {
		[dataTask setTaskDescription:@"SearchTask"];
	}
	[dataTask resume];
}

#pragma mark - Content Provider

/*
 * Simple ContentProvider for requesting and housekeeping ressources
 */
- (void)requestLocalImagePathForImageURL:(NSURL *)imageURL withImageBlock:(ImageBlock)imageBlock {
	[self.contentProvider requestLocalImagePathForImageURL:imageURL withImageBlock:imageBlock];
}

#pragma mark - Parsing

/*
 * Parsing header-infos from HTTPResponseHeader and store into dataModel
 */
- (void)parseHTTPHeader:(NSDictionary *)httpHeader withRequestType:(RFTRequestTypes)requestType {
	
	RFTObjectList *listModel = nil;
	
	switch (requestType) {
		case RFTRequestTypePopular: listModel = self.popularList; break;
		case RFTRequestTypeSearch: listModel = self.searchList; break;
	}
	
	[listModel setCurrentPage:[httpHeader objectForKey:kPaginationCurrentPageKey]];
	[listModel setPageCount:[httpHeader objectForKey:kPaginationPageCountKey]];
	[listModel setLimit:[httpHeader objectForKey:kPaginationLimitKey]];
	[listModel setItemCount:[httpHeader objectForKey:kPaginationItemCountKey]];
}

/*
 * Parsing specific JSON and store into dataModel
 */
- (void)parseJSONData:(NSData *)JSONData withRequestType:(RFTRequestTypes)requestType {
	
	// Check if JSONdata is empty
	if (!JSONData) {
		NSLog(@"Error parsing data response");
		return;
	}
	
	// Parse JSON and check if there is a result
	NSError *error = nil;
	id jsonResult = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:&error];
	// Not the best way to validate the JSON... Has to be changed
	// We just know we are prepared for an array
	if (!jsonResult || ![jsonResult isKindOfClass:[NSArray class]]) {
		NSLog(@"Error parsing JSON: %@", error);
		return;
	}
	
	// Iterate over all dicts and create model
	for (NSDictionary *dict in jsonResult) {
		switch (requestType) {
			case RFTRequestTypePopular:
				[self.popularList addPopularModel:[[RFTPopularModel alloc] initWithDictionary:dict]];
				break;
				
			case RFTRequestTypeSearch:
				[self.searchList addsearchModel:[[RFTSearchModel alloc] initWithDictionary:dict]];
				break;
		}
	}
}

@end
