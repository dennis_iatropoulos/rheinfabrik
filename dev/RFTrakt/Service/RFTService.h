//
//  RFTService.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RFTContentProvider.h"
#import "RFTObjectList.h"
#import "RFTPopularList.h"
#import "RFTSearchList.h"
#import "RFTPopularModel.h"
#import "RFTSearchModel.h"

#import "RFTPopularImages.h"
#import "RFTPopularBanner.h"
#import "RFTPopularPoster.h"
#import "RFTPopularLogo.h"
#import "RFTPopularFanart.h"

#import "RFTSearchMovie.h"
#import "RFTSearchImages.h"
#import "RFTSearchFanart.h"
#import "RFTSearchPoster.h"

typedef void (^CompletionBlock)(void);
typedef void (^ReturnBlock)(RFTObjectList *objectList);
typedef void (^ErrorBlock)(NSError *error);

@interface RFTService : NSObject

- (void)cleanPopularMovies;
- (void)cleanSearchMovies;

- (void)requestMorePopularMoviesWithReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock;
- (void)requestMoreSearchMoviesForTextquery:(NSString *)textquery withReturnBlock:(ReturnBlock)returnBlock withErrorBlock:(ErrorBlock)errorBlock;

- (void)requestLocalImagePathForImageURL:(NSURL *)imageURL withImageBlock:(ImageBlock)imageBlock;

@end
