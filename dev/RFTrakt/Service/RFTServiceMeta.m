//
//  RFTServiceMeta.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTServiceMeta.h"

#define kHostPopularKey	 @"RFTHostPopular"
#define kHostSearchKey	 @"RFTHostSearch"
#define kLimitPopularKey @"RFTLimitPopular"
#define kLimitSearchKey	 @"RFTLimitSearch"
#define kParamsKey		 @"RFTParams"

@interface RFTServiceMeta ()

@property (nonatomic, strong) NSDictionary *serviceList;

@end

@implementation RFTServiceMeta

- (instancetype)init {
	self = [super init];
	if (self) {
		[self parseList];
	}
	return self;
}

/*
 * Read meta-infos for service settings from plist in bundle
 */
- (void)parseList {
	
	self.serviceList = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RFTServiceMeta" ofType:@"plist"]];
	
	self.hostPopular =	[self.serviceList objectForKey:kHostPopularKey];
	self.hostSearch =	[self.serviceList objectForKey:kHostSearchKey];
	self.limitPopular = [self.serviceList objectForKey:kLimitPopularKey];
	self.limitSearch =  [self.serviceList objectForKey:kLimitSearchKey];
	self.params =		[self.serviceList objectForKey:kParamsKey];
}

- (NSString *)description {
	NSMutableString *desc = [NSMutableString stringWithString:@"RFTServiceMeta:\n"];
	[desc appendFormat:@"\thostPopular='%@'\n", self.hostPopular];
	[desc appendFormat:@"\thostSearch='%@'\n", self.hostSearch];
	[desc appendFormat:@"\tlimitPopular='%@'\n", self.limitPopular];
	[desc appendFormat:@"\tlimitSearch='%@'\n", self.limitSearch];
	[self.params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
		[desc appendFormat:@"{params %@ = %@}\n", key, obj];
	}];
	return desc;
}

@end
