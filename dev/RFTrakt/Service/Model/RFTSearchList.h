//
//  RFTSearchList.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RFTObjectList.h"

@class RFTSearchModel;

@interface RFTSearchList : RFTObjectList

@property (nonatomic, strong) NSMutableArray *searchModels;

- (void)addsearchModel:(RFTSearchModel *)searchModel;

@end
