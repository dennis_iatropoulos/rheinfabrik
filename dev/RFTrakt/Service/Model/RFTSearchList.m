//
//  RFTSearchList.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTSearchList.h"
#import "RFTSearchModel.h"

@implementation RFTSearchList

- (NSMutableArray *)searchModels {
	if (!_searchModels) {
		_searchModels = [[NSMutableArray alloc] init];
	}
	return _searchModels;
}

- (void)addsearchModel:(RFTSearchModel *)searchModel {
	[self.searchModels addObject:searchModel];
}

- (NSString *)description {
	NSMutableString *desc = [NSMutableString stringWithString:@"RFTSearchList:\n"];
	[desc appendFormat:@"\tcurrentPage='%@'\n", self.currentPage];
	[desc appendFormat:@"\tpageCount='%@'\n", self.pageCount];
	[desc appendFormat:@"\tlimit='%@'\n", self.limit];
	[desc appendFormat:@"\titemCount='%@'\n", self.itemCount];
	[self.searchModels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[desc appendFormat:@"{popularModels%lu=%@}\n", (unsigned long)idx, obj];
	}];
	return desc;
}

@end
