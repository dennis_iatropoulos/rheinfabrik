//
//  RFTPopularList.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTPopularList.h"
#import "RFTPopularModel.h"

@implementation RFTPopularList

- (NSMutableArray *)popularModels {
	if (!_popularModels) {
		_popularModels = [[NSMutableArray alloc] init];
	}
	return _popularModels;
}

- (void)addPopularModel:(RFTPopularModel *)popularModel {
	[self.popularModels addObject:popularModel];
}

- (NSString *)description {
	NSMutableString *desc = [NSMutableString stringWithString:@"RFTPopularList:\n"];
	[desc appendFormat:@"\tcurrentPage='%@'\n", self.currentPage];
	[desc appendFormat:@"\tpageCount='%@'\n", self.pageCount];
	[desc appendFormat:@"\tlimit='%@'\n", self.limit];
	[desc appendFormat:@"\titemCount='%@'\n", self.itemCount];
	[self.popularModels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[desc appendFormat:@"{popularModels%lu=%@}\n", (unsigned long)idx, obj];
	}];
	return desc;
}

@end
