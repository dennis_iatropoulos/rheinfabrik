//
//  RFTObjectList.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RFTObjectList : NSObject

@property (nonatomic, strong) NSString *currentPage;
@property (nonatomic, strong) NSString *pageCount;
@property (nonatomic, strong) NSString *limit;
@property (nonatomic, strong) NSString *itemCount;

@end
