//
//  RFTSearchModel.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTSearchModel.h"
#import "RFTSearchMovie.h"


NSString *const kRFTSearchModelType = @"type";
NSString *const kRFTSearchModelScore = @"score";
NSString *const kRFTSearchModelMovie = @"movie";


@interface RFTSearchModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTSearchModel

@synthesize type = _type;
@synthesize score = _score;
@synthesize movie = _movie;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.type = [self objectOrNilForKey:kRFTSearchModelType fromDictionary:dict];
            self.score = [[self objectOrNilForKey:kRFTSearchModelScore fromDictionary:dict] doubleValue];
            self.movie = [RFTSearchMovie modelObjectWithDictionary:[dict objectForKey:kRFTSearchModelMovie]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.type forKey:kRFTSearchModelType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.score] forKey:kRFTSearchModelScore];
    [mutableDict setValue:[self.movie dictionaryRepresentation] forKey:kRFTSearchModelMovie];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.type = [aDecoder decodeObjectForKey:kRFTSearchModelType];
    self.score = [aDecoder decodeDoubleForKey:kRFTSearchModelScore];
    self.movie = [aDecoder decodeObjectForKey:kRFTSearchModelMovie];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_type forKey:kRFTSearchModelType];
    [aCoder encodeDouble:_score forKey:kRFTSearchModelScore];
    [aCoder encodeObject:_movie forKey:kRFTSearchModelMovie];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTSearchModel *copy = [[RFTSearchModel alloc] init];
    
    if (copy) {

        copy.type = [self.type copyWithZone:zone];
        copy.score = self.score;
        copy.movie = [self.movie copyWithZone:zone];
    }
    
    return copy;
}


@end
