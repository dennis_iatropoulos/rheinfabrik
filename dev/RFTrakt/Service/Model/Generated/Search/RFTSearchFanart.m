//
//  RFTSearchFanart.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTSearchFanart.h"


NSString *const kRFTSearchFanartFull = @"full";
NSString *const kRFTSearchFanartThumb = @"thumb";
NSString *const kRFTSearchFanartMedium = @"medium";


@interface RFTSearchFanart ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTSearchFanart

@synthesize full = _full;
@synthesize thumb = _thumb;
@synthesize medium = _medium;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.full = [self objectOrNilForKey:kRFTSearchFanartFull fromDictionary:dict];
            self.thumb = [self objectOrNilForKey:kRFTSearchFanartThumb fromDictionary:dict];
            self.medium = [self objectOrNilForKey:kRFTSearchFanartMedium fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.full forKey:kRFTSearchFanartFull];
    [mutableDict setValue:self.thumb forKey:kRFTSearchFanartThumb];
    [mutableDict setValue:self.medium forKey:kRFTSearchFanartMedium];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.full = [aDecoder decodeObjectForKey:kRFTSearchFanartFull];
    self.thumb = [aDecoder decodeObjectForKey:kRFTSearchFanartThumb];
    self.medium = [aDecoder decodeObjectForKey:kRFTSearchFanartMedium];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_full forKey:kRFTSearchFanartFull];
    [aCoder encodeObject:_thumb forKey:kRFTSearchFanartThumb];
    [aCoder encodeObject:_medium forKey:kRFTSearchFanartMedium];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTSearchFanart *copy = [[RFTSearchFanart alloc] init];
    
    if (copy) {

        copy.full = [self.full copyWithZone:zone];
        copy.thumb = [self.thumb copyWithZone:zone];
        copy.medium = [self.medium copyWithZone:zone];
    }
    
    return copy;
}


@end
