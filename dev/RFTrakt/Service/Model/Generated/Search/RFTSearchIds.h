//
//  RFTSearchIds.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RFTSearchIds : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double tmdb;
@property (nonatomic, strong) NSString *slug;
@property (nonatomic, assign) double trakt;
@property (nonatomic, strong) NSString *imdb;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
