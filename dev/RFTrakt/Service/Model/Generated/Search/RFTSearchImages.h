//
//  RFTSearchImages.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RFTSearchFanart, RFTSearchPoster;

@interface RFTSearchImages : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) RFTSearchFanart *fanart;
@property (nonatomic, strong) RFTSearchPoster *poster;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
