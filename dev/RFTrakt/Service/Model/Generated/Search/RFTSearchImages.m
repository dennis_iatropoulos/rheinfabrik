//
//  RFTSearchImages.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTSearchImages.h"
#import "RFTSearchFanart.h"
#import "RFTSearchPoster.h"


NSString *const kRFTSearchImagesFanart = @"fanart";
NSString *const kRFTSearchImagesPoster = @"poster";


@interface RFTSearchImages ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTSearchImages

@synthesize fanart = _fanart;
@synthesize poster = _poster;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.fanart = [RFTSearchFanart modelObjectWithDictionary:[dict objectForKey:kRFTSearchImagesFanart]];
            self.poster = [RFTSearchPoster modelObjectWithDictionary:[dict objectForKey:kRFTSearchImagesPoster]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.fanart dictionaryRepresentation] forKey:kRFTSearchImagesFanart];
    [mutableDict setValue:[self.poster dictionaryRepresentation] forKey:kRFTSearchImagesPoster];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.fanart = [aDecoder decodeObjectForKey:kRFTSearchImagesFanart];
    self.poster = [aDecoder decodeObjectForKey:kRFTSearchImagesPoster];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_fanart forKey:kRFTSearchImagesFanart];
    [aCoder encodeObject:_poster forKey:kRFTSearchImagesPoster];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTSearchImages *copy = [[RFTSearchImages alloc] init];
    
    if (copy) {

        copy.fanart = [self.fanart copyWithZone:zone];
        copy.poster = [self.poster copyWithZone:zone];
    }
    
    return copy;
}


@end
