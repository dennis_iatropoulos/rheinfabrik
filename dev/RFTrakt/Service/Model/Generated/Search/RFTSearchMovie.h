//
//  RFTSearchMovie.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RFTSearchIds, RFTSearchImages;

@interface RFTSearchMovie : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) RFTSearchIds *ids;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double year;
@property (nonatomic, strong) RFTSearchImages *images;
@property (nonatomic, strong) NSString *overview;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
