//
//  RFTSearchIds.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTSearchIds.h"


NSString *const kRFTSearchIdsTmdb = @"tmdb";
NSString *const kRFTSearchIdsSlug = @"slug";
NSString *const kRFTSearchIdsTrakt = @"trakt";
NSString *const kRFTSearchIdsImdb = @"imdb";


@interface RFTSearchIds ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTSearchIds

@synthesize tmdb = _tmdb;
@synthesize slug = _slug;
@synthesize trakt = _trakt;
@synthesize imdb = _imdb;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tmdb = [[self objectOrNilForKey:kRFTSearchIdsTmdb fromDictionary:dict] doubleValue];
            self.slug = [self objectOrNilForKey:kRFTSearchIdsSlug fromDictionary:dict];
            self.trakt = [[self objectOrNilForKey:kRFTSearchIdsTrakt fromDictionary:dict] doubleValue];
            self.imdb = [self objectOrNilForKey:kRFTSearchIdsImdb fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tmdb] forKey:kRFTSearchIdsTmdb];
    [mutableDict setValue:self.slug forKey:kRFTSearchIdsSlug];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trakt] forKey:kRFTSearchIdsTrakt];
    [mutableDict setValue:self.imdb forKey:kRFTSearchIdsImdb];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tmdb = [aDecoder decodeDoubleForKey:kRFTSearchIdsTmdb];
    self.slug = [aDecoder decodeObjectForKey:kRFTSearchIdsSlug];
    self.trakt = [aDecoder decodeDoubleForKey:kRFTSearchIdsTrakt];
    self.imdb = [aDecoder decodeObjectForKey:kRFTSearchIdsImdb];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tmdb forKey:kRFTSearchIdsTmdb];
    [aCoder encodeObject:_slug forKey:kRFTSearchIdsSlug];
    [aCoder encodeDouble:_trakt forKey:kRFTSearchIdsTrakt];
    [aCoder encodeObject:_imdb forKey:kRFTSearchIdsImdb];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTSearchIds *copy = [[RFTSearchIds alloc] init];
    
    if (copy) {

        copy.tmdb = self.tmdb;
        copy.slug = [self.slug copyWithZone:zone];
        copy.trakt = self.trakt;
        copy.imdb = [self.imdb copyWithZone:zone];
    }
    
    return copy;
}


@end
