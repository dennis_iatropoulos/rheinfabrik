//
//  RFTSearchMovie.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTSearchMovie.h"
#import "RFTSearchIds.h"
#import "RFTSearchImages.h"


NSString *const kRFTSearchMovieIds = @"ids";
NSString *const kRFTSearchMovieTitle = @"title";
NSString *const kRFTSearchMovieYear = @"year";
NSString *const kRFTSearchMovieImages = @"images";
NSString *const kRFTSearchMovieOverview = @"overview";


@interface RFTSearchMovie ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTSearchMovie

@synthesize ids = _ids;
@synthesize title = _title;
@synthesize year = _year;
@synthesize images = _images;
@synthesize overview = _overview;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ids = [RFTSearchIds modelObjectWithDictionary:[dict objectForKey:kRFTSearchMovieIds]];
            self.title = [self objectOrNilForKey:kRFTSearchMovieTitle fromDictionary:dict];
            self.year = [[self objectOrNilForKey:kRFTSearchMovieYear fromDictionary:dict] doubleValue];
            self.images = [RFTSearchImages modelObjectWithDictionary:[dict objectForKey:kRFTSearchMovieImages]];
            self.overview = [self objectOrNilForKey:kRFTSearchMovieOverview fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.ids dictionaryRepresentation] forKey:kRFTSearchMovieIds];
    [mutableDict setValue:self.title forKey:kRFTSearchMovieTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.year] forKey:kRFTSearchMovieYear];
    [mutableDict setValue:[self.images dictionaryRepresentation] forKey:kRFTSearchMovieImages];
    [mutableDict setValue:self.overview forKey:kRFTSearchMovieOverview];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.ids = [aDecoder decodeObjectForKey:kRFTSearchMovieIds];
    self.title = [aDecoder decodeObjectForKey:kRFTSearchMovieTitle];
    self.year = [aDecoder decodeDoubleForKey:kRFTSearchMovieYear];
    self.images = [aDecoder decodeObjectForKey:kRFTSearchMovieImages];
    self.overview = [aDecoder decodeObjectForKey:kRFTSearchMovieOverview];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_ids forKey:kRFTSearchMovieIds];
    [aCoder encodeObject:_title forKey:kRFTSearchMovieTitle];
    [aCoder encodeDouble:_year forKey:kRFTSearchMovieYear];
    [aCoder encodeObject:_images forKey:kRFTSearchMovieImages];
    [aCoder encodeObject:_overview forKey:kRFTSearchMovieOverview];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTSearchMovie *copy = [[RFTSearchMovie alloc] init];
    
    if (copy) {

        copy.ids = [self.ids copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.year = self.year;
        copy.images = [self.images copyWithZone:zone];
        copy.overview = [self.overview copyWithZone:zone];
    }
    
    return copy;
}


@end
