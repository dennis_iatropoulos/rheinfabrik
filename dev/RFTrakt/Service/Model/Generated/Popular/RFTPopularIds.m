//
//  RFTPopularIds.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTPopularIds.h"


NSString *const kRFTPopularIdsTmdb = @"tmdb";
NSString *const kRFTPopularIdsSlug = @"slug";
NSString *const kRFTPopularIdsTrakt = @"trakt";
NSString *const kRFTPopularIdsImdb = @"imdb";


@interface RFTPopularIds ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTPopularIds

@synthesize tmdb = _tmdb;
@synthesize slug = _slug;
@synthesize trakt = _trakt;
@synthesize imdb = _imdb;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tmdb = [[self objectOrNilForKey:kRFTPopularIdsTmdb fromDictionary:dict] doubleValue];
            self.slug = [self objectOrNilForKey:kRFTPopularIdsSlug fromDictionary:dict];
            self.trakt = [[self objectOrNilForKey:kRFTPopularIdsTrakt fromDictionary:dict] doubleValue];
            self.imdb = [self objectOrNilForKey:kRFTPopularIdsImdb fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tmdb] forKey:kRFTPopularIdsTmdb];
    [mutableDict setValue:self.slug forKey:kRFTPopularIdsSlug];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trakt] forKey:kRFTPopularIdsTrakt];
    [mutableDict setValue:self.imdb forKey:kRFTPopularIdsImdb];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tmdb = [aDecoder decodeDoubleForKey:kRFTPopularIdsTmdb];
    self.slug = [aDecoder decodeObjectForKey:kRFTPopularIdsSlug];
    self.trakt = [aDecoder decodeDoubleForKey:kRFTPopularIdsTrakt];
    self.imdb = [aDecoder decodeObjectForKey:kRFTPopularIdsImdb];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tmdb forKey:kRFTPopularIdsTmdb];
    [aCoder encodeObject:_slug forKey:kRFTPopularIdsSlug];
    [aCoder encodeDouble:_trakt forKey:kRFTPopularIdsTrakt];
    [aCoder encodeObject:_imdb forKey:kRFTPopularIdsImdb];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTPopularIds *copy = [[RFTPopularIds alloc] init];
    
    if (copy) {

        copy.tmdb = self.tmdb;
        copy.slug = [self.slug copyWithZone:zone];
        copy.trakt = self.trakt;
        copy.imdb = [self.imdb copyWithZone:zone];
    }
    
    return copy;
}


@end
