//
//  RFTPopularThumb.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RFTPopularThumb : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *full;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
