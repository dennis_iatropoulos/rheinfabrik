//
//  RFTPopularImages.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTPopularImages.h"
#import "RFTPopularLogo.h"
#import "RFTPopularClearart.h"
#import "RFTPopularPoster.h"
#import "RFTPopularFanart.h"
#import "RFTPopularThumb.h"
#import "RFTPopularBanner.h"


NSString *const kRFTPopularImagesLogo = @"logo";
NSString *const kRFTPopularImagesClearart = @"clearart";
NSString *const kRFTPopularImagesPoster = @"poster";
NSString *const kRFTPopularImagesFanart = @"fanart";
NSString *const kRFTPopularImagesThumb = @"thumb";
NSString *const kRFTPopularImagesBanner = @"banner";


@interface RFTPopularImages ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTPopularImages

@synthesize logo = _logo;
@synthesize clearart = _clearart;
@synthesize poster = _poster;
@synthesize fanart = _fanart;
@synthesize thumb = _thumb;
@synthesize banner = _banner;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.logo = [RFTPopularLogo modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesLogo]];
            self.clearart = [RFTPopularClearart modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesClearart]];
            self.poster = [RFTPopularPoster modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesPoster]];
            self.fanart = [RFTPopularFanart modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesFanart]];
            self.thumb = [RFTPopularThumb modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesThumb]];
            self.banner = [RFTPopularBanner modelObjectWithDictionary:[dict objectForKey:kRFTPopularImagesBanner]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.logo dictionaryRepresentation] forKey:kRFTPopularImagesLogo];
    [mutableDict setValue:[self.clearart dictionaryRepresentation] forKey:kRFTPopularImagesClearart];
    [mutableDict setValue:[self.poster dictionaryRepresentation] forKey:kRFTPopularImagesPoster];
    [mutableDict setValue:[self.fanart dictionaryRepresentation] forKey:kRFTPopularImagesFanart];
    [mutableDict setValue:[self.thumb dictionaryRepresentation] forKey:kRFTPopularImagesThumb];
    [mutableDict setValue:[self.banner dictionaryRepresentation] forKey:kRFTPopularImagesBanner];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.logo = [aDecoder decodeObjectForKey:kRFTPopularImagesLogo];
    self.clearart = [aDecoder decodeObjectForKey:kRFTPopularImagesClearart];
    self.poster = [aDecoder decodeObjectForKey:kRFTPopularImagesPoster];
    self.fanart = [aDecoder decodeObjectForKey:kRFTPopularImagesFanart];
    self.thumb = [aDecoder decodeObjectForKey:kRFTPopularImagesThumb];
    self.banner = [aDecoder decodeObjectForKey:kRFTPopularImagesBanner];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_logo forKey:kRFTPopularImagesLogo];
    [aCoder encodeObject:_clearart forKey:kRFTPopularImagesClearart];
    [aCoder encodeObject:_poster forKey:kRFTPopularImagesPoster];
    [aCoder encodeObject:_fanart forKey:kRFTPopularImagesFanart];
    [aCoder encodeObject:_thumb forKey:kRFTPopularImagesThumb];
    [aCoder encodeObject:_banner forKey:kRFTPopularImagesBanner];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTPopularImages *copy = [[RFTPopularImages alloc] init];
    
    if (copy) {

        copy.logo = [self.logo copyWithZone:zone];
        copy.clearart = [self.clearart copyWithZone:zone];
        copy.poster = [self.poster copyWithZone:zone];
        copy.fanart = [self.fanart copyWithZone:zone];
        copy.thumb = [self.thumb copyWithZone:zone];
        copy.banner = [self.banner copyWithZone:zone];
    }
    
    return copy;
}


@end
