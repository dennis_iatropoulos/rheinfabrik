//
//  RFTPopularImages.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RFTPopularLogo, RFTPopularClearart, RFTPopularPoster, RFTPopularFanart, RFTPopularThumb, RFTPopularBanner;

@interface RFTPopularImages : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) RFTPopularLogo *logo;
@property (nonatomic, strong) RFTPopularClearart *clearart;
@property (nonatomic, strong) RFTPopularPoster *poster;
@property (nonatomic, strong) RFTPopularFanart *fanart;
@property (nonatomic, strong) RFTPopularThumb *thumb;
@property (nonatomic, strong) RFTPopularBanner *banner;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
