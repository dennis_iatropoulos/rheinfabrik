//
//  RFTPopularPoster.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTPopularPoster.h"


NSString *const kRFTPopularPosterFull = @"full";
NSString *const kRFTPopularPosterThumb = @"thumb";
NSString *const kRFTPopularPosterMedium = @"medium";


@interface RFTPopularPoster ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTPopularPoster

@synthesize full = _full;
@synthesize thumb = _thumb;
@synthesize medium = _medium;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.full = [self objectOrNilForKey:kRFTPopularPosterFull fromDictionary:dict];
            self.thumb = [self objectOrNilForKey:kRFTPopularPosterThumb fromDictionary:dict];
            self.medium = [self objectOrNilForKey:kRFTPopularPosterMedium fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.full forKey:kRFTPopularPosterFull];
    [mutableDict setValue:self.thumb forKey:kRFTPopularPosterThumb];
    [mutableDict setValue:self.medium forKey:kRFTPopularPosterMedium];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.full = [aDecoder decodeObjectForKey:kRFTPopularPosterFull];
    self.thumb = [aDecoder decodeObjectForKey:kRFTPopularPosterThumb];
    self.medium = [aDecoder decodeObjectForKey:kRFTPopularPosterMedium];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_full forKey:kRFTPopularPosterFull];
    [aCoder encodeObject:_thumb forKey:kRFTPopularPosterThumb];
    [aCoder encodeObject:_medium forKey:kRFTPopularPosterMedium];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTPopularPoster *copy = [[RFTPopularPoster alloc] init];
    
    if (copy) {

        copy.full = [self.full copyWithZone:zone];
        copy.thumb = [self.thumb copyWithZone:zone];
        copy.medium = [self.medium copyWithZone:zone];
    }
    
    return copy;
}


@end
