//
//  RFTPopularFanart.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTPopularFanart.h"


NSString *const kRFTPopularFanartFull = @"full";
NSString *const kRFTPopularFanartThumb = @"thumb";
NSString *const kRFTPopularFanartMedium = @"medium";


@interface RFTPopularFanart ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTPopularFanart

@synthesize full = _full;
@synthesize thumb = _thumb;
@synthesize medium = _medium;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.full = [self objectOrNilForKey:kRFTPopularFanartFull fromDictionary:dict];
            self.thumb = [self objectOrNilForKey:kRFTPopularFanartThumb fromDictionary:dict];
            self.medium = [self objectOrNilForKey:kRFTPopularFanartMedium fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.full forKey:kRFTPopularFanartFull];
    [mutableDict setValue:self.thumb forKey:kRFTPopularFanartThumb];
    [mutableDict setValue:self.medium forKey:kRFTPopularFanartMedium];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.full = [aDecoder decodeObjectForKey:kRFTPopularFanartFull];
    self.thumb = [aDecoder decodeObjectForKey:kRFTPopularFanartThumb];
    self.medium = [aDecoder decodeObjectForKey:kRFTPopularFanartMedium];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_full forKey:kRFTPopularFanartFull];
    [aCoder encodeObject:_thumb forKey:kRFTPopularFanartThumb];
    [aCoder encodeObject:_medium forKey:kRFTPopularFanartMedium];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTPopularFanart *copy = [[RFTPopularFanart alloc] init];
    
    if (copy) {

        copy.full = [self.full copyWithZone:zone];
        copy.thumb = [self.thumb copyWithZone:zone];
        copy.medium = [self.medium copyWithZone:zone];
    }
    
    return copy;
}


@end
