//
//  RFTPopularModel.m
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "RFTPopularModel.h"
#import "RFTPopularIds.h"
#import "RFTPopularImages.h"


NSString *const kRFTPopularModelRating = @"rating";
NSString *const kRFTPopularModelIds = @"ids";
NSString *const kRFTPopularModelReleased = @"released";
NSString *const kRFTPopularModelTrailer = @"trailer";
NSString *const kRFTPopularModelRuntime = @"runtime";
NSString *const kRFTPopularModelTitle = @"title";
NSString *const kRFTPopularModelOverview = @"overview";
NSString *const kRFTPopularModelAvailableTranslations = @"available_translations";
NSString *const kRFTPopularModelHomepage = @"homepage";
NSString *const kRFTPopularModelUpdatedAt = @"updated_at";
NSString *const kRFTPopularModelYear = @"year";
NSString *const kRFTPopularModelLanguage = @"language";
NSString *const kRFTPopularModelTagline = @"tagline";
NSString *const kRFTPopularModelGenres = @"genres";
NSString *const kRFTPopularModelCertification = @"certification";
NSString *const kRFTPopularModelImages = @"images";
NSString *const kRFTPopularModelVotes = @"votes";


@interface RFTPopularModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RFTPopularModel

@synthesize rating = _rating;
@synthesize ids = _ids;
@synthesize released = _released;
@synthesize trailer = _trailer;
@synthesize runtime = _runtime;
@synthesize title = _title;
@synthesize overview = _overview;
@synthesize availableTranslations = _availableTranslations;
@synthesize homepage = _homepage;
@synthesize updatedAt = _updatedAt;
@synthesize year = _year;
@synthesize language = _language;
@synthesize tagline = _tagline;
@synthesize genres = _genres;
@synthesize certification = _certification;
@synthesize images = _images;
@synthesize votes = _votes;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rating = [[self objectOrNilForKey:kRFTPopularModelRating fromDictionary:dict] doubleValue];
            self.ids = [RFTPopularIds modelObjectWithDictionary:[dict objectForKey:kRFTPopularModelIds]];
            self.released = [self objectOrNilForKey:kRFTPopularModelReleased fromDictionary:dict];
            self.trailer = [self objectOrNilForKey:kRFTPopularModelTrailer fromDictionary:dict];
            self.runtime = [[self objectOrNilForKey:kRFTPopularModelRuntime fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kRFTPopularModelTitle fromDictionary:dict];
            self.overview = [self objectOrNilForKey:kRFTPopularModelOverview fromDictionary:dict];
            self.availableTranslations = [self objectOrNilForKey:kRFTPopularModelAvailableTranslations fromDictionary:dict];
            self.homepage = [self objectOrNilForKey:kRFTPopularModelHomepage fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kRFTPopularModelUpdatedAt fromDictionary:dict];
            self.year = [[self objectOrNilForKey:kRFTPopularModelYear fromDictionary:dict] doubleValue];
            self.language = [self objectOrNilForKey:kRFTPopularModelLanguage fromDictionary:dict];
            self.tagline = [self objectOrNilForKey:kRFTPopularModelTagline fromDictionary:dict];
            self.genres = [self objectOrNilForKey:kRFTPopularModelGenres fromDictionary:dict];
            self.certification = [self objectOrNilForKey:kRFTPopularModelCertification fromDictionary:dict];
            self.images = [RFTPopularImages modelObjectWithDictionary:[dict objectForKey:kRFTPopularModelImages]];
            self.votes = [[self objectOrNilForKey:kRFTPopularModelVotes fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rating] forKey:kRFTPopularModelRating];
    [mutableDict setValue:[self.ids dictionaryRepresentation] forKey:kRFTPopularModelIds];
    [mutableDict setValue:self.released forKey:kRFTPopularModelReleased];
    [mutableDict setValue:self.trailer forKey:kRFTPopularModelTrailer];
    [mutableDict setValue:[NSNumber numberWithDouble:self.runtime] forKey:kRFTPopularModelRuntime];
    [mutableDict setValue:self.title forKey:kRFTPopularModelTitle];
    [mutableDict setValue:self.overview forKey:kRFTPopularModelOverview];
    NSMutableArray *tempArrayForAvailableTranslations = [NSMutableArray array];
    for (NSObject *subArrayObject in self.availableTranslations) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAvailableTranslations addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAvailableTranslations addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAvailableTranslations] forKey:kRFTPopularModelAvailableTranslations];
    [mutableDict setValue:self.homepage forKey:kRFTPopularModelHomepage];
    [mutableDict setValue:self.updatedAt forKey:kRFTPopularModelUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.year] forKey:kRFTPopularModelYear];
    [mutableDict setValue:self.language forKey:kRFTPopularModelLanguage];
    [mutableDict setValue:self.tagline forKey:kRFTPopularModelTagline];
    NSMutableArray *tempArrayForGenres = [NSMutableArray array];
    for (NSObject *subArrayObject in self.genres) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForGenres addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForGenres addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForGenres] forKey:kRFTPopularModelGenres];
    [mutableDict setValue:self.certification forKey:kRFTPopularModelCertification];
    [mutableDict setValue:[self.images dictionaryRepresentation] forKey:kRFTPopularModelImages];
    [mutableDict setValue:[NSNumber numberWithDouble:self.votes] forKey:kRFTPopularModelVotes];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rating = [aDecoder decodeDoubleForKey:kRFTPopularModelRating];
    self.ids = [aDecoder decodeObjectForKey:kRFTPopularModelIds];
    self.released = [aDecoder decodeObjectForKey:kRFTPopularModelReleased];
    self.trailer = [aDecoder decodeObjectForKey:kRFTPopularModelTrailer];
    self.runtime = [aDecoder decodeDoubleForKey:kRFTPopularModelRuntime];
    self.title = [aDecoder decodeObjectForKey:kRFTPopularModelTitle];
    self.overview = [aDecoder decodeObjectForKey:kRFTPopularModelOverview];
    self.availableTranslations = [aDecoder decodeObjectForKey:kRFTPopularModelAvailableTranslations];
    self.homepage = [aDecoder decodeObjectForKey:kRFTPopularModelHomepage];
    self.updatedAt = [aDecoder decodeObjectForKey:kRFTPopularModelUpdatedAt];
    self.year = [aDecoder decodeDoubleForKey:kRFTPopularModelYear];
    self.language = [aDecoder decodeObjectForKey:kRFTPopularModelLanguage];
    self.tagline = [aDecoder decodeObjectForKey:kRFTPopularModelTagline];
    self.genres = [aDecoder decodeObjectForKey:kRFTPopularModelGenres];
    self.certification = [aDecoder decodeObjectForKey:kRFTPopularModelCertification];
    self.images = [aDecoder decodeObjectForKey:kRFTPopularModelImages];
    self.votes = [aDecoder decodeDoubleForKey:kRFTPopularModelVotes];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_rating forKey:kRFTPopularModelRating];
    [aCoder encodeObject:_ids forKey:kRFTPopularModelIds];
    [aCoder encodeObject:_released forKey:kRFTPopularModelReleased];
    [aCoder encodeObject:_trailer forKey:kRFTPopularModelTrailer];
    [aCoder encodeDouble:_runtime forKey:kRFTPopularModelRuntime];
    [aCoder encodeObject:_title forKey:kRFTPopularModelTitle];
    [aCoder encodeObject:_overview forKey:kRFTPopularModelOverview];
    [aCoder encodeObject:_availableTranslations forKey:kRFTPopularModelAvailableTranslations];
    [aCoder encodeObject:_homepage forKey:kRFTPopularModelHomepage];
    [aCoder encodeObject:_updatedAt forKey:kRFTPopularModelUpdatedAt];
    [aCoder encodeDouble:_year forKey:kRFTPopularModelYear];
    [aCoder encodeObject:_language forKey:kRFTPopularModelLanguage];
    [aCoder encodeObject:_tagline forKey:kRFTPopularModelTagline];
    [aCoder encodeObject:_genres forKey:kRFTPopularModelGenres];
    [aCoder encodeObject:_certification forKey:kRFTPopularModelCertification];
    [aCoder encodeObject:_images forKey:kRFTPopularModelImages];
    [aCoder encodeDouble:_votes forKey:kRFTPopularModelVotes];
}

- (id)copyWithZone:(NSZone *)zone
{
    RFTPopularModel *copy = [[RFTPopularModel alloc] init];
    
    if (copy) {

        copy.rating = self.rating;
        copy.ids = [self.ids copyWithZone:zone];
        copy.released = [self.released copyWithZone:zone];
        copy.trailer = [self.trailer copyWithZone:zone];
        copy.runtime = self.runtime;
        copy.title = [self.title copyWithZone:zone];
        copy.overview = [self.overview copyWithZone:zone];
        copy.availableTranslations = [self.availableTranslations copyWithZone:zone];
        copy.homepage = [self.homepage copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.year = self.year;
        copy.language = [self.language copyWithZone:zone];
        copy.tagline = [self.tagline copyWithZone:zone];
        copy.genres = [self.genres copyWithZone:zone];
        copy.certification = [self.certification copyWithZone:zone];
        copy.images = [self.images copyWithZone:zone];
        copy.votes = self.votes;
    }
    
    return copy;
}


@end
