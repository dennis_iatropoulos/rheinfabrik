//
//  RFTPopularModel.h
//
//  Created by   on 11.03.16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RFTPopularIds, RFTPopularImages;

@interface RFTPopularModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double rating;
@property (nonatomic, strong) RFTPopularIds *ids;
@property (nonatomic, strong) NSString *released;
@property (nonatomic, strong) NSString *trailer;
@property (nonatomic, assign) double runtime;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *overview;
@property (nonatomic, strong) NSArray *availableTranslations;
@property (nonatomic, strong) NSString *homepage;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, assign) double year;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *tagline;
@property (nonatomic, strong) NSArray *genres;
@property (nonatomic, strong) NSString *certification;
@property (nonatomic, strong) RFTPopularImages *images;
@property (nonatomic, assign) double votes;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
