//
//  RFTPopularList.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RFTObjectList.h"

@class RFTPopularModel;

@interface RFTPopularList : RFTObjectList

@property (nonatomic, strong) NSMutableArray *popularModels;

- (void)addPopularModel:(RFTPopularModel *)popularModel;

@end
