//
//  RFTContentProvider.m
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 12.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import "RFTContentProvider.h"

#define kLocalImagePathName @"imageCache"

@interface RFTContentProvider ()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation RFTContentProvider

- (instancetype)init {
	self = [super init];
	if (self) {
		[self prepareSession];
	}
	return self;
}

- (void)prepareSession {
	
	NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
	sessionConfig.allowsCellularAccess = YES;
	sessionConfig.timeoutIntervalForRequest = 30.0;
	sessionConfig.timeoutIntervalForResource = 60.0;
	
	self.session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
}

/*
 * Downloading image from given "imageURL", saves it to cacheDirectory and returns localImagePath in "ImageBlock"
 */
- (void)requestLocalImagePathForImageURL:(NSURL *)imageURL withImageBlock:(ImageBlock)imageBlock {
	
	// If the imageURL is empty return nil.
	if (!imageURL) {
		NSLog(@"Missing imageURL");
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			if (imageBlock) {
				imageBlock(nil);
			}
		}];
		return;
	}
	
	// If the image for the given "imageURL" has already been downloaded, trigger "imageBlock" with localImagePath
	NSString *imagefilePath = [NSString stringWithFormat:@"%@/%i@2x.jpg", [self imageCachePath], (int)[imageURL hash]];
	if ([[NSFileManager defaultManager] fileExistsAtPath:imagefilePath]) {
		if (imageBlock) {
			imageBlock(imagefilePath);
		}
		return;
	}
	
	// If the image for the given "imageURL" is not downloaded yet, start request and trigger "imageBlock" with localImagePath
	NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
		if (!error) {
			if (((NSHTTPURLResponse *)response).statusCode == 200) {
				NSError *writeError;
				// Save image to local folder
				[[NSData dataWithContentsOfURL:location] writeToFile:imagefilePath options:NSDataWritingAtomic error:&writeError];
				if (!writeError) {
					[[NSOperationQueue mainQueue] addOperationWithBlock:^{
						if (imageBlock) {
							imageBlock(imagefilePath);
						}
					}];
				} else {
					NSLog(@"Error: %@ Saving Ressource for URL:%@", error, location);
					[[NSOperationQueue mainQueue] addOperationWithBlock:^{
						if (imageBlock) {
							imageBlock(nil);
						}
					}];
				}
			} else {
				// Handle Statuscodes
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					if (imageBlock) {
						imageBlock(nil);
					}
				}];
			}
		} else {
			NSLog(@"Error: %@ Requesting Ressource for URL:%@", error, location);
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				if (imageBlock) {
					imageBlock(nil);
				}
			}];
		}
	}];
	
	[downloadTask resume];
}

/*
 * returns the path for caching ressources
 * If path not exists method will create it
 */
- (NSString *)imageCachePath {
	
	// Check if folder already was created. If not create it.
	NSString *cachePath = [NSString stringWithFormat:@"%@/%@", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0], kLocalImagePathName];
	
	BOOL isDir = YES;
	if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir]) {
		NSError *error;
		[[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:&error];
		if (error) {
			NSLog(@"Error creating path at %@", cachePath);
		}
	}
	
	return cachePath;
}

@end
