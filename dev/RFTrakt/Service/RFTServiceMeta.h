//
//  RFTServiceMeta.h
//  RFTrakt
//
//  Created by Zaphod Beeblebrox on 11.03.16.
//  Copyright © 2016 Dennis Iatropoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RFTServiceMeta : NSObject

@property (nonatomic, strong) NSString *hostPopular;
@property (nonatomic, strong) NSString *hostSearch;
@property (nonatomic, strong) NSString *limitPopular;
@property (nonatomic, strong) NSString *limitSearch;
@property (nonatomic, strong) NSDictionary *params;

@end
